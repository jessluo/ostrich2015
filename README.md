OSTRICH 2015
========

This is the data processing and analysis code for data from a cruise in the Straits of Florida using the _In Situ_ Ichthyoplankton Imaging System (ISIIS; Cowen and Guigand 2008).  
The cruise took place in June 2015, and is part of a project funded by the National Science Foundation (NSF). 

###Code Contact:
Jessica Luo  
Email: jluo [at] ucar.edu

Kelly Robinson  
Email: kelly.robinson [at] louisiana.edu


###PI contact:
Robert K Cowen and Su Sponaugle  
Hatfield Marine Science Center  
Oregon State University  
Newport, OR

You can access this repo through SSH or HTTPS.
